const fs = require('fs');
const bcrypt = require('bcrypt');
const _ = require('lodash');
let checkpoint = 0, res;
let givenCheckpoint = 0;
if (res = /^--checkpoint=(\d+)$/.exec(process.argv[2])) {
  givenCheckpoint = res[1];
  process.argv.splice(2, 1);
}
async function run() {
  /**
   * @type{Array}
   */
  let data = JSON.parse(fs.readFileSync(process.argv[2]));
  let lseq = _.groupBy(data, 'sequence');

  for (let seq of Object.keys(lseq)) {
    let types = _.groupBy(lseq[seq], 'type');
    for (let type in types) {
      let at = types[type];
      console.log(`Checking sequence ${seq}-${type} with ${at.length} tickets`);
      let [eos, tickets] = _.partition(at, v => v.content === 'EOS');
      if (!eos[0]) {
        throw new Error(`EOS not found`);
      }
      tickets.push(eos[0]);
      for (let i = 1; i < tickets.length; i++) {
        checkpoint++;
        if (givenCheckpoint > checkpoint) {
          continue;
        }
        let prev = tickets[i - 1];
        let cur = tickets[i];
        let signature = `${prev.signature}-${cur.sequence}-${cur.number}-${cur.date}-${cur.content}`;


        let csig = cur.signature.replace(/^\$2y/, "$2b"); // need to replace 2y with 2b for node bcrypt to accept it
        if (!await bcrypt.compare(signature, csig)) {
          throw new Error(`Sig chain break at ${cur.number}\n${signature}\n${cur.signature}`);
        }
        if (i % 100 == 0) {
          console.log(`${i * 100 / tickets.length}%`);
        }
      }
    }
  }
}

function checkPointCmd() {
  return `${process.argv[0]} ${process.argv[1]} --checkpoint=${checkpoint} ${process.argv[2]}`
}

process.on('SIGINT', function () {
  console.log(`\n\nExiting. Restart from current position with\n${checkPointCmd()}`);
  process.exit();
});


run().catch(e => {
  console.error(e);
  console.log(`\n\nAn error occurred. Restart from current position with\n${checkPointCmd()}`);
  process.exit();
});

