# Pasteque sigcheck

An internal tool used to verify the signatures of fiscal tickets in pasteque as exported by the fiscal API.

## Usage
Install dependencies:

```
npm install
```

Extract the exported zip-file. Run the program as

```
node index.js fiscal_export-.....txt
```
